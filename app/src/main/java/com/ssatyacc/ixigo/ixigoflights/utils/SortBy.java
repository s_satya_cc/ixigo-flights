package com.ssatyacc.ixigo.ixigoflights.utils;

/**
 * Created by ssatyacc on 2/24/17.
 */

public enum SortBy {
    LANDING_TIME,
    STARTING_TIME,
    PRICE_LOW_TO_HIGH,
    PRICE_HIGH_TO_LOW
}